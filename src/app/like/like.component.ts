import { Component, Input } from '@angular/core';
import { faHeart } from '@fortawesome/free-solid-svg-icons';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'app-like',
  templateUrl: './like.component.html',
  styleUrls: ['./like.component.css']
})
export class LikeComponent {

  faHeart = faHeart;
  // tslint:disable-next-line: no-input-rename
  @Input('likesCount') likesCount: number;
  // tslint:disable-next-line: no-input-rename
  @Input('isActive') isActive: boolean;

  onClick() {
      this.likesCount += (this.isActive) ? -1 : 1;
      // tslint:disable-next-line: triple-equals
      this.isActive = !this.isActive;     
   }

   onMouseEnter() {
     this.isActive = (!this.isActive) ? true : false;  
   }

   onMouseLeave() {
    this.isActive = (this.isActive) ? true : false;
   }

}
